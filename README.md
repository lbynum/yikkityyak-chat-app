# Yikkity Yak Chat App

This is a very simple project done as a means to quickly learn the basics of how to build and deploy a web application using Google Firebase!

Firebase is a cloud service that allows developers to quickly build and deploy apps with useful functionality (analytics, databases, messaging, crash reporting, etc.) without having to manage the typical server infrastructure. Or so they say!

And it turns out they are right. With only a few lines of code (and lots of help from online walkthroughs and tutorials), we can do some pretty cool things!

We've taken Yik Yak, the now-deceased social media discussion platform, as inspiration.

# Functionality

Overall we've made a simple chat/discussion app, where we can post and upvote messages. To learn how Firebase works, we've implemented a few notable features.

## Authentication

You can't post until you've signed in!

![](examples/auth_fail_demo.gif)

## Per-user Authentication

You can sign in using Google!

![](examples/auth_demo.gif)

## Live Upvoting and Ranking

You can upvote messages, and their scores and rankings are shown in real-time!

![](examples/voting_demo.gif)

# Future Work

The point of this project was to learn, but there are all kinds of other things we could implement, like commenting, filters, sorting toggles, and perhaps most notably--limiting each user to only one upvote per post!
