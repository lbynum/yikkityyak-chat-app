'use strict';

function NewsAggregator() {

  // shortcuts to Elements
  this.messageList = document.getElementById('messages');
  this.messageForm = document.getElementById('message-form');
  this.messageInput = document.getElementById('message');
  this.submitButton = document.getElementById('submit');
  this.userPic = document.getElementById('user-pic');
  this.userName = document.getElementById('user-name');
  this.signInButton = document.getElementById('sign-in');
  this.signOutButton = document.getElementById('sign-out');
  this.signInSnackbar = document.getElementById('must-signin-snackbar');

  // save messages as they are posted
  this.messageForm.addEventListener('submit', this.savePost.bind(this));

  // Listen to signing in and out
  this.signOutButton.addEventListener('click', this.signOut.bind(this));
  this.signInButton.addEventListener('click', this.signIn.bind(this));

  // toggle for the button
  var buttonTogglingHandler = this.toggleButton.bind(this);
  // if messageInput changes toggle the button
  this.messageInput.addEventListener('keyup', buttonTogglingHandler);
  this.messageInput.addEventListener('change', buttonTogglingHandler);

  // initialize firebase
  this.initFirebase();
}

// initialize firebase
NewsAggregator.prototype.initFirebase = function() {
  // shortcuts to Firebase SDK features
  this.auth = firebase.auth();
  this.database = firebase.database();

  // initiate firebause auth and listen to auth state changes
  this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};

// enables or disables the submit button
NewsAggregator.prototype.toggleButton = function() {
  // enable button if there is message input, otherwise disable
  if (this.messageInput.value) {
    this.submitButton.removeAttribute('disabled');
  } else {
    this.submitButton.setAttribute('disabled', 'true');
  }
};

// ------------------------------ Authentication ------------------------------

// sign in using GoogleAuthProvider
NewsAggregator.prototype.signIn = function() {
  // Sign in Firebase using popup auth and Google as the identity provider
  var provider = new firebase.auth.GoogleAuthProvider();
  this.auth.signInWithPopup(provider);
};

// sign out
NewsAggregator.prototype.signOut = function() {
  // sign out of Firebase
  this.auth.signOut();
};

// toggle buttons and display to show user is signed in or out
NewsAggregator.prototype.onAuthStateChanged = function(user) {
  // if signed in display profile picture and username
  if (user) {
    // get username and picture
    var profilePicUrl = user.photoUrl;
    var userName = user.displayName;

    // set username and picture
    this.userPic.style.backgroundImage = 'url(' + profilePicUrl + ')';
    this.userName.textContent = userName;

    // show username and picture in UI
    this.userName.removeAttribute('hidden');
    this.userPic.removeAttribute('hidden');

    // show sign-out button
    this.signOutButton.removeAttribute('hidden');

    // hide sign-in button
    this.signInButton.setAttribute('hidden', 'true');

    // load existing posts
    this.loadPosts();
  } else { // if user is signed out, hide user-specific display items
    // hide username and picture in UI
    this.userName.setAttribute('hidden', 'true');
    this.userPic.setAttribute('hidden', 'true');

    // hide sign-out button
    this.signOutButton.setAttribute('hidden', 'true');

    // show sign-in button
    this.signInButton.removeAttribute('hidden');
  }
};

// returns true if user is signed in and false if not
//   also displays a sign-in popup if the user is not signed in
NewsAggregator.prototype.checkSignedInWithMessage = function() {
  // return true if the user is signed in
  if (this.auth.currentUser) {
    return true;
  }

  // else display sign in message to the user using a snackbar
  var data = {
    message: 'You must sign-in before posting',
    timeout: 2000
  };
  this.signInSnackbar.MaterialSnackbar.showSnackbar(data);
  return false;
};

// reset MaterialTextField (used when resetting the message text field)
//  after a post is made
NewsAggregator.resetMaterialTextfield = function(element) {
  element.value = '';
  element.parentNode.MaterialTextfield.boundUpdateClassesHandler();
};

// ---------------------------------- Posting ----------------------------------
NewsAggregator.prototype.savePost = function(event) {
  // prevent link from opening the URL
  event.preventDefault();

  // check that user entered a message and is signed in
  if (this.messageInput.value && this.checkSignedInWithMessage()) {
    // get current user information
    var currentUser = this.auth.currentUser;

    // push new post to Firebase database
    this.postsRef.push({
      // set children data
      name: currentUser.displayName,
      text: this.messageInput.value,
      photoUrl: currentUser.photoURL,
      upvoteCount: 0
    }).then(function() {
      // reset the text field
      NewsAggregator.resetMaterialTextfield(this.messageInput);

      // reset the POST button
      this.toggleButton();
    }.bind(this)).catch(function(error) {
      console.error('Error writing new message to database', error);
    });
  }
};

// load existing posts and listen for new posts
NewsAggregator.prototype.loadPosts = function() {
  // make reference to the /messages/ database path
  this.postsRef = this.database.ref('messages');
  
  // remove all previous listeners
  this.postsRef.off();

  // function to display posts
  var setPost = function(data) {
    // Grab information from post data
    var val = data.val();

    // Post information in window
    this.displayPost(data.key, val.name, val.text, val.photoUrl, val.upvoteCount);
  }.bind(this);

  // listen for and display changes in posts
  this.postsRef.on('child_added', setPost);
  this.postsRef.on('child_changed', setPost);
};

// template for posts
NewsAggregator.POST_TEMPLATE = 
  '<div class="message-container">' + 
    '<div class="spacing"><div class="pic"></div></div>' + // user picture
    '<div class="message"></div>' + // message text
    '<div class="name"></div>' + // username
    '<button class="upvoteCount" onclick=""></button>' // message upvoteCount 
  '</div>';

// display a post in the UI
NewsAggregator.prototype.displayPost = function(userId, name, text, picUrl, upvoteCount) {
  // get DOM element for the post
  var div = document.getElementById(userId);

  // if the element for the post does not exist yet, create it
  if (!div) {
    // create div element
    var container = document.createElement('div');

    // set inner HTML using post template
    container.innerHTML = NewsAggregator.POST_TEMPLATE;
    div = container.firstChild;

    // set user id
    div.setAttribute('id', userId);

    // get upvoteCount element and set upvoteCount value
    var upvoteButton = div.querySelector('.upvoteCount');
    upvoteButton.innerHTML = upvoteCount;

    // listen for click and update upvoteCount value once when clicked
    upvoteButton.addEventListener('click', function(event) {
      // get reference to post that was clicked
      var upvoteCountRef = firebase.database().ref('messages/' + userId + '/upvoteCount');

      // increment that post's value once
      upvoteCountRef.once('value').then(function(snapshot) {
        var newUpvoteCount = snapshot.val() + 1;
        firebase.database().ref('messages/' + userId).update({'/upvoteCount' : newUpvoteCount});
      });
    }); 
  }
  
  // if user has a picture, set their picture as the background image
  if (picUrl) {
    div.querySelector('.pic').style.backgroundImage = 'url(' + picUrl + ')';
  }
  // get name element and set text to username
  div.querySelector('.name').textContent = name;

  // get upvoteCount element and set value to upvoteCount
  div.querySelector('.upvoteCount').innerHTML = upvoteCount;

  // get message element (the url)
  var messageElement = div.querySelector('.message');

  // set message content to entered text
  messageElement.textContent = text;  
  
  // display elements in order sorted by their upvoteCount
  if (this.messageList.childNodes.length <= 3) { 
    // list contains no messages, so simply append
    this.messageList.appendChild(div);
  } else {
    // loop through post list and insert before first element ranked higher
    var i;
    var notInserted = true;
    for (i = this.messageList.childNodes.length-1; i>2; i--) {
      // get current count from div element
      var currentUpvoteCount = this.messageList.childNodes[i].childNodes[3].innerHTML;
      if (upvoteCount > currentUpvoteCount) {
        this.messageList.insertBefore(div, this.messageList.childNodes[i]);
        notInserted = false;
      } 
    }
    if (notInserted) {
      this.messageList.appendChild(div);
    }
  }

  // show the message card in the UI
  setTimeout(function() {div.classList.add('visible')}, 1);
  this.messageInput.focus();
};

// ----------------------------- Initializing Window -----------------------------
// when window refreshes, reload aggregator
window.onload = function() {
  window.newsAggregator = new NewsAggregator();
};





